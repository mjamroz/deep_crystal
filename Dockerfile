FROM debian:jessie

ENV API_PORT 9000
ENV OMP_NUM_THREADS=2
ENV http_proxy http://proxy.embl.fr:3128
ENV https_proxy https://proxy.embl.fr:3128

ENV DEBIAN_FRONTEND noninteractive
ENV LC_ALL en_US.UTF-8 
ENV LANG en_US.UTF-8
ENV LANGUAGE en_US:en

RUN apt-get update && apt-get install -y --no-install-recommends git locales \
    apt-utils python-sqlite python-skimage python build-essential python-numpy \
    libopencv-dev python-setuptools sudo python-dev supervisor python-opencv \
    libopencv-calib3d2.4 libopencv-gpu2.4 libopencv-imgproc2.4 \
    libopencv-core2.4 libopenblas-dev libopenblas-base \
    libopencv-ocl2.4 libopencv-stitching2.4 libopencv-superres2.4 \
    libopencv-ts2.4 libopencv-videostab2.4
RUN echo "en_US.UTF-8 UTF-8" > /etc/locale.gen && \
    locale-gen en_US.UTF-8 && \
    dpkg-reconfigure locales && \
    /usr/sbin/update-locale LANG=en_US.UTF-8


WORKDIR /usr/local
RUN git config --global http.sslVerify false && \
    git clone https://github.com/dmlc/mxnet.git --recursive  && \
    git clone https://bitbucket.org/mjamroz/protein-crystal-image-recognition.git && \
    sed -i "s|, 9000),|, $API_PORT),|g" protein-crystal-image-recognition/simple_post_server.py
WORKDIR /usr/local/mxnet 

RUN sed -i 's/USE_BLAS = atlas/USE_BLAS = openblas/g' make/config.mk && \
    make -j10 && cd python && sudo python setup.py install

RUN apt-get remove  -y libopencv-dev libatlas-base-dev libopencv-dev \
    libopenblas-dev python-dev && apt-get autoremove -y && \
    apt-get clean && apt-get autoclean && ln /dev/null /dev/raw1394

RUN rm -rf /etc/supervisor/conf.d/* && echo '[program:api_server]\n\
command=/usr/local/protein-crystal-image-recognition/simple_post_server.py\n\
stdout_logfile=/var/log/supervisor/%(program_name)s.log\n\
stderr_logfile=/var/log/supervisor/%(program_name)s.log\n\
autorestart=true\n\
directory=/usr/local/protein-crystal-image-recognition/\n\
user=root\n\
autostart=true\n' > /etc/supervisor/conf.d/server_api.conf

EXPOSE $API_PORT
ENV PYTHONPATH=/usr/local/mxnet/:$PYTHONPATH
CMD ["/usr/bin/supervisord", "-n"]
