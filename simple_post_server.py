#!/usr/bin/env python
# server source : https://gist.github.com/UniIsland/3346170
import os
import BaseHTTPServer
import urllib
import shutil
import mimetypes
import re
import mxnet as mx
import numpy as np
import tempfile
from skimage import io, transform
try:
    from cStringIO import StringIO
except ImportError:
    from StringIO import StringIO

prefix = "Inception_BN299NoCenter-0"
num_round = 1231
mean_img = mx.nd.load("mean_224.nd")["mean_img"]
synset = [l.strip().split()[1] for l in open('synset.txt').readlines()]
model = mx.model.FeedForward.load(prefix, num_round, ctx=mx.cpu(), numpy_batch_size=1)


def PreprocessImage(path):
    # load image
    img = io.imread(path)
    # we crop image from center
    short_egde = min(img.shape[:2])
    yy = int((img.shape[0] - short_egde) / 2)
    xx = int((img.shape[1] - short_egde) / 2)
    crop_img = img[yy : yy + short_egde, xx : xx + short_egde]
    #crop_img = img
    # resize to 224, 224
    resized_img = transform.resize(crop_img, (224, 224))
    # convert to numpy.ndarray
    sample = np.asarray(resized_img) * 256
    # swap axes to make image from (224, 224, 4) to (3, 224, 224)
    sample = np.swapaxes(sample, 0, 2)
    sample = np.swapaxes(sample, 1, 2)
    # sub mean
    normed_img = sample - mean_img.asnumpy()
    normed_img.resize(1, 3, 224, 224)
    return normed_img

class SimpleHTTPRequestHandler(BaseHTTPServer.BaseHTTPRequestHandler):
    def do_POST(self):
        """Serve a POST request."""
        out = tempfile.NamedTemporaryFile(delete=False, suffix=".jpg")
        fname = out.name
        r, info = self.deal_post_data(out)
        out.close()
        prob = model.predict(PreprocessImage(out.name))[0]
        pred = np.argsort(prob)[::-1]
        top1 = synset[pred[0]]
        os.unlink(out.name)
        f = StringIO()
        if r:
            f.write('{"score":%13.12f,"class":"%s"}\n'
                    %(prob[pred[0]], synset[pred[0]]))
        else:
            f.write('{"message": "Upload image file."}')
        length = f.tell()
        f.seek(0)
        self.send_response(200)
        self.send_header("Content-type", "application/json")
        self.send_header("Content-Length", str(length))
        self.end_headers()
        if f:
            shutil.copyfileobj(f, self.wfile)
            f.close()

    def deal_post_data(self, out):
        boundary = self.headers.plisttext.split("=")[1]
        remainbytes = int(self.headers['content-length'])
        line = self.rfile.readline()
        remainbytes -= len(line)
        if not boundary in line:
            return (False, "Content NOT begin with boundary")
        line = self.rfile.readline()
        remainbytes -= len(line)
        fn = re.findall(r'Content-Disposition.*name="file"; filename="(.*)"', line)
        if not fn:
            return (False, "Can't find out file name...")
        line = self.rfile.readline()
        remainbytes -= len(line)
        line = self.rfile.readline()
        remainbytes -= len(line)

        preline = self.rfile.readline()
        remainbytes -= len(preline)
        while remainbytes > 0:
            line = self.rfile.readline()
            remainbytes -= len(line)
            if boundary in line:
                preline = preline[0:-1]
                if preline.endswith('\r'):
                    preline = preline[0:-1]
                out.write(preline)
                return (True, "File upload success!")
            else:
                out.write(preline)
                preline = line
        return (False, "Unexpect Ends of data.")


if __name__ == '__main__':
    httpd = BaseHTTPServer.HTTPServer(("", 9000), SimpleHTTPRequestHandler)
    httpd.serve_forever()
