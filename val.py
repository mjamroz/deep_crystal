#!/usr/bin/env python
import mxnet as mx
import logging
import numpy as np
from skimage import io, transform
from sys import argv
prefix = "Inception_BN299NoCenter-0"
num_round = 1231
model = mx.model.FeedForward.load(prefix, num_round, ctx=mx.cpu(), numpy_batch_size=1)
mean_img = mx.nd.load("mean_224.nd")["mean_img"]
fname=argv[1]
synset = [l.strip().split()[1] for l in open('synset.txt').readlines()]

def PreprocessImage(path, show_img=False):
    # load image
    img = io.imread(path)
    # we crop image from center
    short_egde = min(img.shape[:2])
    yy = int((img.shape[0] - short_egde) / 2)
    xx = int((img.shape[1] - short_egde) / 2)
    crop_img = img[yy : yy + short_egde, xx : xx + short_egde]
    #crop_img = img
    # resize to 224, 224
    resized_img = transform.resize(crop_img, (224, 224))
    # convert to numpy.ndarray
    sample = np.asarray(resized_img) * 256
    # swap axes to make image from (224, 224, 4) to (3, 224, 224)
    sample = np.swapaxes(sample, 0, 2)
    sample = np.swapaxes(sample, 1, 2)
    # sub mean
    normed_img = sample - mean_img.asnumpy()
    normed_img.resize(1, 3, 224, 224)
    return normed_img

batch = PreprocessImage(fname, False)
prob = model.predict(batch)[0]
pred = np.argsort(prob)[::-1]
top1 = synset[pred[0]]
print "1st: %13.12f %11s  2nd: %13.12f %11s 3rd: %13.12f %11s | %s" %(prob[pred[0]], synset[pred[0]], prob[pred[1]], synset[pred[1]],prob[pred[2]], synset[pred[2]], argv[1])
