#!/usr/bin/env python
import json
import urllib2
import urllib
import sqlite3
import mxnet as mx
import numpy as np
import os

from skimage import io, transform

CRIMS_API = "http://htxlab-dev:8888/api/v2/"

prefix = "Inception_BN299NoCenter-0"
num_round = 1231
mean_img = mx.nd.load("mean_224.nd")["mean_img"]
synset = [l.strip().split()[1] for l in open('synset.txt').readlines()]
model = mx.model.FeedForward.load(prefix, num_round, ctx=mx.cpu(), numpy_batch_size=1)


conn = sqlite3.connect("predictions.db")
db = conn.cursor()
db.execute("CREATE TABLE IF NOT EXISTS processed(\
        id INTEGER PRIMARY KEY ASC, \
        pred BOOLEAN DEFAULT 0, \
        barcode TEXT, \
        inspection INTEGER, \
        imageurl TEXT, \
        prediction TEXT, \
        date DATE DEFAULT CURRENT_DATE, \
        UNIQUE(barcode, inspection, imageurl) ON CONFLICT IGNORE \
        )")
db.execute("CREATE TABLE IF NOT EXISTS fetched(\
        id INTEGER PRIMARY KEY ASC, \
        barcode TEXT, \
        inspection INTEGER, \
        date DATE DEFAULT CURRENT_DATE \
        )")


def images_fetched(barcode, inspection):
    db.execute("SELECT COUNT(*) FROM fetched WHERE barcode=? \
                AND inspection=?", [barcode, inspection])
    return db.fetchone()[0]

def get_images_list(barcode, inspection):
    images_url = CRIMS_API + "plates/" + str(barcode) + "/inspections/" + \
            str(inspection) + "/images"
    try:
        images_list = urllib2.urlopen(images_url)
        images_data = json.loads(images_list.read())
        db.execute("INSERT INTO fetched(barcode, inspection) VALUES(?,?)",
                [barcode, inspection])
        conn.commit()
        return [row['images']['rawimages'] for row in images_data['data']]
    except:
        return []

def fetch_images_urls():
    limit = 1200
    url_prefix = CRIMS_API + "plates/?limit="+str(limit)+"&page="
    page = 1
    total_pages = 100000000
    while page <= total_pages:
        plates_list = urllib2.urlopen(url_prefix+str(page))
        data = json.loads(plates_list.read())
        total_pages = data['paginator']['total_pages']
        print page

        for plate in data['data']:
            barcode = plate['barcode']
            for insp in plate['inspections']['inspection']:
                inspection = insp['inspection']
                if not images_fetched(barcode, inspection):
                    for image in get_images_list(barcode, inspection):
                        #if image.endswith(".jpg") or image.endswith(".JPG"):
                        #print image, barcode, inspection
                        db.execute("INSERT INTO processed(barcode, inspection, imageurl) VALUES(?,?,?)", [barcode, inspection, image])
                    conn.commit()

        page += 1

def PreprocessImage(path):
    # load image
    img = io.imread(path)
    # we crop image from center
    short_egde = min(img.shape[:2])
    yy = int((img.shape[0] - short_egde) / 2)
    xx = int((img.shape[1] - short_egde) / 2)
    crop_img = img[yy : yy + short_egde, xx : xx + short_egde]
    #crop_img = img
    # resize to 224, 224
    resized_img = transform.resize(crop_img, (224, 224))
    # convert to numpy.ndarray
    sample = np.asarray(resized_img) * 256
    # swap axes to make image from (224, 224, 4) to (3, 224, 224)
    sample = np.swapaxes(sample, 0, 2)
    sample = np.swapaxes(sample, 1, 2)
    # sub mean
    normed_img = sample - mean_img.asnumpy()
    normed_img.resize(1, 3, 224, 224)
    return normed_img

def prediction():
    db.execute("SELECT imageurl,id FROM processed WHERE pred=0")
    for image_d in db.fetchall():
        img_url = image_d[0]
        img_id = image_d[1]
        img_local_path, _ = urllib.urlretrieve(img_url)
        prob = model.predict(PreprocessImage(img_local_path))[0]
        pred = np.argsort(prob)[::-1]
        top1 = synset[pred[0]]
        os.unlink(img_local_path)
        print img_url, top1
        db.execute("UPDATE processed SET prediction=?, pred=1 WHERE id=?", [top1, img_id])
        conn.commit()
if __name__ == '__main__':
    fetch_images_urls()
    prediction()

